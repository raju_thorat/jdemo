package j_demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUitl {
	
	public static List<Map<String, Object>> readExcel(String fileName) throws URISyntaxException, IOException {
		List<Map<String, Object>> data = new ArrayList<>();
		URL resource = Main.class.getClassLoader().getResource(fileName);
		File file = new File(resource.toURI());
		FileInputStream excelFile = new FileInputStream(file);
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();
		int i = 0;
		List<String> headings = new ArrayList<>();
		while (iterator.hasNext()) {

			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			if (i == 0) {
				while (cellIterator.hasNext()) {

					Cell currentCell = cellIterator.next();
					headings.add(currentCell.getStringCellValue());
				}
			} else {
				Map<String, Object> row = new LinkedHashMap<>();
				int j = 0;
				while (cellIterator.hasNext()) {

					Cell currentCell = cellIterator.next();

					switch (currentCell.getCellTypeEnum()) {
					case STRING:
						row.put(headings.get(j), currentCell.getStringCellValue());
						break;

					case NUMERIC:
						row.put(headings.get(j), currentCell.getNumericCellValue());
						break;

					default:
						break;
					}

					j++;
				}

				data.add(row);

			}

			i++;

		}
		return data;
	}

}
