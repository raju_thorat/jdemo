package j_demo;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Main {

	private static final String FILE_NAME = "data.xlsx";

	public static void main(String[] args) throws Exception {

		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("http://www.google.com/");
		driver.manage().window().maximize();

		Thread.sleep(4000);
		List<Map<String, Object>> readExcel = ExcelUitl.readExcel(FILE_NAME);
		for (Map<String, Object> map : readExcel) {
			driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[1]/div/div[1]/input"))
					.sendKeys(map.get("Name").toString());
			driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[3]/center/input[1]")).click();
			driver.navigate().back();
			Thread.sleep(4000);
		}

		// Assert.assertEquals(driver.getTitle(),"Welcome: Mercury Tours","Wrong page");

	}

}
